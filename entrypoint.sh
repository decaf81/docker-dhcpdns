#!/bin/bash -x
# test

set -e

init="/usr/bin/dumb-init"
data_dir=/srv
dhcpd_conf="$data_dir/dhcpd.conf"
dhcpd_leases="$data_dir/dhcpd.leases"
mkdir -p $data_dir
e=1

declare -gA interfacearray
if [ -z "$DOMAIN" ]; then DOMAIN="example.loc";let "e++";echo "DOMAIN variable not set. Using $DOMAIN"; fi
if [ -z "$NAME"]; then NAME="master"; echo "NAME variable not set. Using $NAME"; fi
FQDN=$NAME.$DOMAIN

###NEEDED FUNCTIONS
function manualshit {
  if [ -z "$SUBNET" ]; then SUBNET="192.168.33.0";let "e++";echo "SUBNET variable not set. Using $SUBNET"; fi
  if [ -z "$NETMASK" ]; then NETMASK="255.255.255.0";let "e++";echo "NETMASK variable not set. Using $NETMASK";fi
  HOSTMIN=$(ipcalc $SUBNET/$NETMASK -b | grep HostMin | awk '{ print $2 }')
  HOSTMAX=$(ipcalc $SUBNET/$NETMASK -b | grep HostMax | awk '{ print $2 }')
}

function find_public_if {
  declare -i y
  y=1
  for i in $(find /sys/devices  -maxdepth 4 | grep net | grep -v virtual | grep -v "net$" | awk -F '\\/net/' '{print $2}'); do
    declare -gA $i #Declare array with interface name
    eval $i[1]=$(ip -f inet -o addr show $i | cut -d\  -f 7) # 1 = ip/SUBNET
    tmpcidr=$(eval echo \${$i[1]})
    eval $i[2]=$(eval ipcalc $tmpcidr  | grep HostMin | awk '{ print $2 }') # HostMin
    eval $i[3]=$(eval ipcalc $tmpcidr  | grep HostMax | awk '{ print $2 }') # HostMax
    eval $i[4]=$(eval ipcalc $tmpcidr  | grep Netmask | awk '{ print $2 }') # #NetMask
    eval $i[5]=$(eval ip -f inet -o addr show $i | cut -d\  -f 7 | cut -d/ -f 1) # Own IP
    eval $i[6]=$(eval ipcalc $tmpcidr  | grep Network | awk '{ print $2 }' | cut -d/ -f 1) # Network
    eval $i[7]=$(eval ipcalc $tmpcidr  | grep Broadcast | awk '{ print $2 }' | cut -d/ -f 1) # Broadcast

    echo Public if $i found, with the following setting
    echo CIDR: $(eval echo \${$i[1]})
    echo IP: $(eval echo \${$i[5]})
    echo NETMASK: $(eval echo \${$i[4]})
    echo HostMin: $(eval echo \${$i[2]})
    echo HostMax: $(eval echo \${$i[3]})
    echo My IP is: $(eval echo \${$i[5]})
    echo My network: $(eval echo \${$i[6]})
    echo My broadcast: $(eval echo \${$i[7]})

    echo
    let "y++"
    eval interfacearray[$y]=$i
  done
  echo ${interfacearray[*]}
  if [[ $y -eq 1 ]] ; then echo No interfaces found; exit 1; fi

}

function fill_dhcp_conf_if () {
   tmpinterface=$(echo $1)
   echo
   echo subnet $(eval echo \${$tmpinterface[6]}) netmask $(eval echo \${$tmpinterface[4]}) \{ >> $dhcpd_conf
   echo " " option subnet-mask $(eval echo \${$tmpinterface[4]}) \; >> $dhcpd_conf
   echo " " option broadcast-address $(eval echo \${$tmpinterface[7]}) \; >> $dhcpd_conf
   echo " " range $(eval echo \${$tmpinterface[2]}) $(eval echo \${$tmpinterface[3]}) \; >> $dhcpd_conf
   echo " " option domain-name-servers $(eval echo \${$tmpinterface[5]}) \; >> $dhcpd_conf
   echo " " options routers 192.168.56.254; >> $dhcpd_conf
   echo " " option domain-name \"$DOMAIN\" \; >> $dhcpd_conf
   echo " " option domain-search \"$DOMAIN\" \; >> $dhcpd_conf
   echo \} >> $dhcpd_conf
   echo >> $dhcpd_conf

}

function prepare_key {
 # Niks meer aan veranderen
 if [ ! -d "/root/tmp" ]; then mkdir /root/tmp; fi
 cd /root/tmp
 keyfile=$(dnssec-keygen -a hmac-md5 -b 256 -n HOST local)
 rndckey=$(cat /root/tmp/$keyfile.private | grep Key | cut -c6-)
 rm -f /root/tmp/$keyfile.*
}

function automagic {
  for i in $(echo ${interfacearray[*]}); do fill_dhcp_conf_if $i; done
}

function preparedns {
  rm -f /etc/bind/named.conf
  rm -f /etc/bind/rndc.conf
  cat /root/named.conf.template |  sed -e "s~\${KEY}~$KEY~" -e "s~\${DOMAIN}~$DOMAIN~" -e "s~\${rndckey}~$rndckey~" > /etc/bind/named.conf
  cat /root/rndc.conf.template | sed -e "s~\${rndckey}~$rndckey~" > /etc/bind/rndc.conf
  rm -f $zonefile
  zonefile=/var/lib/bind/$DOMAIN.hosts
  cat /root/zonefile.template  | sed -e "s~\${DOMAIN}~$DOMAIN~" -e  "s~\${FQDN}~$FQDN~" >> $zonefile
  for i in $(echo ${interfacearray[*]}); do
    tmpinterface=$(echo $i)
    echo $NAME A $(eval echo \${$tmpinterface[5]}) >> $zonefile
  done
}


if [ ! -f "$dhcpd_leases" ]; then touch $dhcpd_leases; fi
if [ -z "$KEY" ]; then KEY="Ld/r+A5Zn2oEBUdQvfZEhA==";let "e++";echo "KEY variable not set. Using $KEY"; fi
if [ -z "$DNSIP" ]; then OWNIP=$(ip route get 1 | awk '{print $NF;exit}');let "e++";echo "DNSIP variable not set. Using $OWNIP"; fi

prepare_key
find_public_if
preparedns

cat /root/dhcpd.conf.template |  sed -e "s~\${KEY}~$KEY~" -e "s~\${NETMASK}~$NETMASK~" -e "s~\${SUBNET}~$SUBNET~" -e "s~\${DOMAIN}~$DOMAIN~" -e "s~\${HOSTMIN}~$HOSTMIN~" -e "s~\${HOSTMAX}~$HOSTMAX~"  -e "s~\${OWNIP}~$OWNIP~"> $dhcpd_conf

automagic

dhcpd -cf $dhcpd_conf -lf $dhcpd_leases $dhargs
named

while /bin/true; do
  ps aux |grep dhcpd |grep -q -v grep
  PROCESS_1_STATUS=$?
  ps aux |grep named |grep -q -v grep
  PROCESS_2_STATUS=$?
  # If the greps above find anything, they will exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_2_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit -1
  fi
  sleep 60
done
